import {makeStyles, createStyles} from '@material-ui/core';

export const useStyles = makeStyles((theme) => createStyles({
    
    
    button: {
        marginTop: '10px !important',
        backgroundColor: '#04471C !important',
        borderRadius: '20px !important',
        width: '10%',
        color: '#FDFFFC !important'
    }
    
  }));