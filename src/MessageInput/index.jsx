import React, { useEffect, useState } from 'react';
import { useStyles } from './classes';
import{ TextField, Button } from '@material-ui/core';
import { v4 } from 'uuid';


export default function MessageInput({
  myUser,
  editingMessage,
  handleMessageAction
}) {
  const [inputMessage, setInputMessage] = useState('');
  const classes = useStyles();

  useEffect(() => {
    if (editingMessage) {
      setInputMessage(editingMessage.text)
    }
  }, [editingMessage]);

  const handleChange = (event) => setInputMessage(event.target.value);

  const createMessage = (messageText) => {
    const message = {
      id: v4(),
      userId: myUser.userId,
      user: myUser.user,
      avatar: myUser.avatar,
      text: messageText,
      createdAt: `${new Date()}`,
      editedAt: ''
    };    
    return message;
  };

  const sendMessage = () => {
    if (!inputMessage) return;
    let newMessage;
    if (editingMessage) {
      newMessage = { ...editingMessage, text: inputMessage };
    } else {
      newMessage = createMessage(inputMessage);
    }
    handleMessageAction(newMessage, editingMessage ? 'edit' : 'create');
    setInputMessage('');
  };  

  return (
    <div style={{flexGrow: 1, marginLeft: '20%'}} className="message-input">
      <TextField 
        style={{width: '65%'}}
        id="outlined-basic" 
        label={editingMessage ? editingMessage.text : "Enter your message"}
        value={inputMessage}
        onChange={handleChange} 
        className="message-input-text" 
      />
      <Button style={{
        marginTop: '10px !important',
        backgroundColor: '#04471C !important',
        borderRadius: '20px !important',
        width: '10%',
        color: '#FDFFFC !important'}} variant="contained" className="message-input-button" onClick={sendMessage}>Send &#128389;</Button>
    </div>
  );
}