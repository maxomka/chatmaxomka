import React from 'react';

export default () => <div className="preloader" style = {{display: 'flex', justifyContent: 'center'}}><div className="lds-dual-ring"></div></div>